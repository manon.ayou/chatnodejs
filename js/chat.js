// Initializes socket.io on the client side and emits the message typed into the input box
(function() {
    var  socket  =  io(); //create a global instance of the soicket.io client on the frontend.
    $("form").submit(function(e) {
        e.preventDefault(); // prevents page reloading
        socket.emit("chat message", $("#m").val()); //enable to send messages to the server by emitting the message from the input box.
        $("#m").val("");
    return  true;
});
})();
