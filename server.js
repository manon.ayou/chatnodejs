//Require the express moule
const express = require("express");

//create a new express application
const app = express()

//require the http module
const http = require("http").Server(app)

// require the socket.io module
const io = require("socket.io");

const port = 500;

//create an event listener
const socket = io(http);

//To listen to new connection or deconnection events (messages)
socket.on("connection", (socket) => {
    console.log("user connected");
    socket.on("disconnect", () => {
        console.log("Disconnected")
    })
});



//wire up the server to listen to our port 500
http.listen(port, () => {
    console.log("connected to port: " + port)
});

socket.on("chat message", function (msg) {
    console.log("message: " + msg);
    //broadcast message to everyone in port:5000 except yourself.
    socket.broadcast.emit("received", { message: msg }); // but the message author should not receive it ?
});
    });

